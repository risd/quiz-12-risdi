const request = require('supertest');
const { expect } = require('chai');

const BASE_URL = ("https://reqres.in/");
const ENDPOINT = ("api/users/2");
const myName = ("Yohanes Risdi Mawan Nugroho");
const myJob = ("Software Quality Assurance");
const payload = {name: myName, job: myJob};

function updateUser(payload) {
    return request(BASE_URL)
        .put(ENDPOINT)
        .send(payload);
}

describe('[PUT] Update user', () => {
    it('Response status code should be 200', async () => {
        const res = await updateUser(payload);
        expect(res.statusCode).to.eq(200);
        console.log(res.statusCode);
        console.log(res.headers['content-type']);
        console.log(res.body);
    });
    it('Response header should have content-type application/json; charset=utf-8 ', async () => {
        const res = await updateUser(payload);
        expect(res.headers['content-type']).to.eq("application/json; charset=utf-8");
    });
    it('Response body should have all keys', async () => {
        const res = await updateUser(payload);
        expect(res.body).to.have.all.keys('name', 'job', 'updatedAt');
    });
    it('Response data name should equal with request data name', async () => {
        const res = await updateUser(payload);
        expect(res.body['name']).to.eq(myName);
    });
    it('Response data job should equal with request data job', async () => {
        const res = await updateUser(payload);
        expect(res.body['job']).to.eq(myJob);
    });
});