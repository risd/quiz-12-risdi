const request = require('supertest');
const { expect } = require('chai');

const BASE_URL = ("https://reqres.in/");
const ENDPOINT = ("api/users");
const myName = ("Yohanes Risdi Mawan Nugroho");
const myJob = ("Software Quality Assurance");
const payload = {name: myName, job: myJob};

function createUser(payload) {
    return request(BASE_URL)
        .post(ENDPOINT)
        .send(payload);
}

describe('[POST] create a new user', () => {
    it('Response status code should be 201', async () => {
        const res = await createUser(payload);
        expect(res.statusCode).to.eq(201);
        console.log(res.statusCode);
        console.log(res.headers['content-type']);
        console.log(res.body);
    });
    it('Response header should have content-type application/json; charset=utf-8 ', async () => {
        const res = await createUser(payload);
        expect(res.headers['content-type']).to.eq("application/json; charset=utf-8");
    });
    it('Response body should have all keys', async () => {
        const res = await createUser(payload);
        expect(res.body).to.have.all.keys('name', 'job', 'id', 'createdAt');
    });
    it('Response data name should equal with request data name', async () => {
        const res = await createUser(payload);
        expect(res.body['name']).to.eq(myName);
    });
    it('Response data job should equal with request data job', async () => {
        const res = await createUser(payload);
        expect(res.body['job']).to.eq(myJob);
    });
});