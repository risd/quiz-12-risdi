const request = require('supertest');
const { expect } = require('chai');

const BASE_URL = ("https://reqres.in/");
const ENDPOINT = ("api/users/2");

function deleteUser(){
    return request(BASE_URL)
        .delete(ENDPOINT)
};

describe('[DELETE] user', () => {
    it('Response status code should be 204', async () => {
        const res = await deleteUser();
        expect(res.statusCode).to.eq(204);
        console.log(res.statusCode);
    });
});