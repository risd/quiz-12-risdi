const request = require('supertest');
const { expect } = require('chai');

const BASE_URL = ("https://reqres.in/");
const ENDPOINT = ("api/users?page=2");

function getUserList(){
    return request(BASE_URL)
        .get(ENDPOINT)
};

describe('[GET] user list', () => {
    it('Response status code should be 200', async () => {
        const res = await getUserList();
        expect(res.statusCode).to.eq(200);
        console.log(res.statusCode);
        console.log(res.headers['content-type']);
        console.log(res.body);
    });
    it('Response header should have content-type application/json; charset=utf-8 ', async () => {
        const res = await getUserList();
        expect(res.headers['content-type']).to.eq("application/json; charset=utf-8");
    });
    it('Response body should have all keys', async () => {
        const res = await getUserList();
        expect(res.body).to.have.all.keys("data", "page", "per_page", "support", "total", "total_pages");
    });
    it('Response body data should have all keys', async () => {
        const res = await getUserList();
        expect(res.body.data[0]).to.have.all.keys("id", "email", "first_name", "last_name", "avatar");
        expect(res.body.data[1]).to.have.all.keys("id", "email", "first_name", "last_name", "avatar");
        expect(res.body.data[2]).to.have.all.keys("id", "email", "first_name", "last_name", "avatar");
        expect(res.body.data[3]).to.have.all.keys("id", "email", "first_name", "last_name", "avatar");
        expect(res.body.data[4]).to.have.all.keys("id", "email", "first_name", "last_name", "avatar");
        expect(res.body.data[5]).to.have.all.keys("id", "email", "first_name", "last_name", "avatar");
    });
    it('Response data support should have all keys', async () => {
        const res = await getUserList();
        expect(res.body['support']).to.have.all.keys("url", "text");
    });
});